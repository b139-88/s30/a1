const express = require('express');
const mongoose = require('mongoose');

const PORT = process.env.PORT || 8888;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect('mongodb://raymond:Mdb9189174@173.82.39.218:49155/Users?authSource=admin', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('Successfully connected!'))

let userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model("Task", userSchema );

app.post("/signup", (req, res) => {
    User.findOne({
        username: req.body.username
    }, (err, result) => {
        if (result != null && result.name == req.body.name) {
            return res.send(`Username ${req.body.username} already exists.`);
        } else {
            let newUser = new User(req.body);

            newUser.save((err, savedUser) => {
                if (err){
                    return console.error(err);
                } else {
                    return res.send(savedUser);
                }
            });
        }
    });
});

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));